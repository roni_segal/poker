#include "Player.h"
#include <iostream>

int Player::HR[] = {};
bool Player::isHrInit = false;
int Player:: player_id_counter = 1;

Player::Player() : table(nullptr), dealerCount(0), bigBlindCount(0),
				   smallBlindCount(0), is_fold(false), is_dealer(false), is_big_blind(false), is_small_blind(false),
                   is_normal_pos(false),
                   is_flop_revieald(false), is_turn_revieald(false), is_river_revieald(false), fold_on_river(0),
                   fold_on_turn(0), fold_on_flop(0), fold_on_hand(0),
                   roundsPlayed(0), money(0), id(player_id_counter++)
{
	if(isHrInit == false)
	{
		printf("Loading HandRanks.DAT file...");
		memset(HR, 0, sizeof(HR));
		FILE * fin = fopen("HandRanks.dat", "rb");
		size_t bytesread = fread(HR, sizeof(HR), 1, fin);	// get the HandRank Array
		fclose(fin);
		printf("complete.\n\n");
		isHrInit = true;
	}
}

int Player::LookupHand(int* pCards)
{
	int p = HR[53 + *pCards++];
	p = HR[p + *pCards++];
	p = HR[p + *pCards++];
	p = HR[p + *pCards++];
	p = HR[p + *pCards++];
	p = HR[p + *pCards++];
	return HR[p + *pCards++];
}

Player::~Player()
{
}

void Player::sit(Table* table)
{
	this->table = table;
	money = startMoney;
}

void Player::infoDealer()
{
	is_dealer = true;
	InfoDealer();
}

void Player::infoBigBlind()
{
	is_big_blind = true;
	InfoBigBlind();
}

void Player::infoSmallBlind()
{
	is_small_blind = true;
	InfoSmallBlind();
}

void Player::infoNormalPos()
{
	is_normal_pos = true;
	InfoNormalPos();
}

void Player::infoRoundStart()
{
	is_fold = false;
	is_normal_pos = false;
	is_dealer = false;
	is_big_blind = false;
	is_small_blind = false;
	is_flop_revieald = false;
	is_turn_revieald = false;
	is_river_revieald = false;
	roundsPlayed++;
}

void Player::infoCardsDrown(Card card1, Card card2)
{
	hand_card1 = card1;
	hand_card2 = card2;
	InfoCardsDrown(card1, card2);
}

void Player::putMoney(const int amount)
{
	money -= amount;
}

void Player::takeMoney(const int amount)
{
	money += amount;
}

int Player::play()
{
	const int ans = Play();
	std::cout << "player: " << id << " do: " << ans << std::endl;
	if (ans == -1)
		Fold();
	// TOOD: collect more data from the move
	return ans;
}

void Player::infoReviealFlop(const std::vector<Card>& cards)
{
	is_flop_revieald = true;
	InfoReviealFlop(cards);
}

void Player::infoReviealTurn(const Card& card)
{
	is_turn_revieald = true;
	InfoReviealTurn(card);
}

void Player::infoReviealRiver(const Card& card)
{
	is_river_revieald = true;
	InfoReviealRiver(card);
}

int Player::getHandRank() const
{
	const std::vector<Card>& cards = table->getTableCards();
	if (cards.size() == 5) {
		int arr[7] =
		{
			cards[0].toInt(),
			cards[1].toInt(),
			cards[2].toInt(),
			cards[3].toInt(),
			cards[4].toInt(),
			hand_card1.toInt(),
			hand_card2.toInt() 
		};
		return LookupHand(arr);
	}
	return -1;
}

bool Player::isDealer() const
{
	return is_dealer;
}

bool Player::isBigBlind() const
{
	return is_big_blind;
}

bool Player::isSmallBlind() const
{
	return is_small_blind;
}

int Player::getDealerCount() const
{
	return dealerCount;
}

int Player::getBigBlindsCount() const
{
	return bigBlindCount;
}

int Player::getSmallBlindsCount() const
{
	return smallBlindCount;
}

bool Player::isFlopRevieald() const
{
	return is_flop_revieald;
}

bool Player::isTurnRevieald() const
{
	return is_turn_revieald;
}

bool Player::isRiverRevieald() const
{
	return is_river_revieald;
}

int Player::getId() const
{
	return id;
}

void Player::infoWinner(int winnerId)
{
	InfoWinner(winnerId);
}

int Player::getMoney() const
{
	return money;
}

void Player::infoShowDown(int playerId, int handRank)
{
	InfoShowDown(playerId, handRank);
}

bool Player::isFold() const
{
	return is_fold;
}

void Player::Fold()
{
	is_fold = true;
	if (isRiverRevieald())
		fold_on_river++;
	else if (isTurnRevieald())
		fold_on_turn++;
	else if (isFlopRevieald())
	{
		fold_on_flop++;
	}
	else
	{
		fold_on_hand++;
	}
}
