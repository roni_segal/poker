#pragma once
#include "Player.h"
#include <SFML/Graphics.hpp>
#include <thread>

class GuiPlayer : public Player
{
public:
	GuiPlayer();
	~GuiPlayer();
	void infoPlayerAction(int i, int action) override;
protected:
	void InfoWinner(int winnerId) override;
	void InfoRoundStart() override;
	void InfoNormalPos() override;
	void InfoDealer() override;
	void InfoBigBlind() override;
	void InfoSmallBlind() override;
	void InfoReviealFlop(const std::vector<Card>& cards) override;
	void InfoReviealTurn(const Card& card) override;
	void InfoReviealRiver(const Card& card) override;
	void InfoCardsDrown(const Card& card, const Card& deal_card) override;
	void MoneyPut(const int small_blind) override;
	int Play() override;
	void InfoShowDown(int playerId, int handRank) override;

private:
	sf::RenderWindow* window;
	bool table_cards_changed;
	void drawMyCards();
	void drawTableCards();
	void draw();
	void GameLoop();
	std::thread window_thread;
	sf::Font font;
	sf::Text textGameState;
	sf::Texture card1Texture;
	sf::Texture card2Texture;
	sf::Sprite card1Sprite;
	sf::Sprite card2Sprite;

	std::vector<Card> table_cards;
	std::vector<sf::Texture> table_cards_texture;
	std::vector<sf::Sprite> table_cards_sprites;
};

