#pragma once
#include "Player.h"
class HumanTextPlayer :
	public Player
{
public:
	HumanTextPlayer();
	~HumanTextPlayer();
	void infoPlayerAction(int i, int action) override;
protected:
	void InfoRoundStart() override;
	void InfoDealer() override;
	void InfoBigBlind() override;
	void InfoSmallBlind() override;
	void InfoNormalPos() override;
	void InfoCardsDrown(const Card& card1, const Card& card2) override;
	void MoneyPut(const int small_blind) override;
	int Play() override;

	void InfoReviealFlop(const std::vector<Card>& cards) override;
	void InfoReviealTurn(const Card& card) override;
	void InfoReviealRiver(const Card& card) override;
	void InfoWinner(int winnerId) override;
	void InfoShowDown(int playerId, int handRank) override;
private:
};

