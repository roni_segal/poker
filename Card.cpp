#include "Card.h"
#include <map>

// init static vars:
const char Card::NUM_TO_CHAR[] = { 'A','2','3','4','5','6','7','8','9','T','J','Q','K' };
const std::unordered_map<std::string, int> Card::CARD_ID = {
	{ "2c", 0 },{ "2d", 1 },{ "2h", 2 },{ "2s", 3 },
	{ "3c", 4 },{ "3d", 5 },{ "3h", 6 },{ "3s", 7 },
	{ "4c", 8 },{ "4d", 9 },{ "4h", 10 },{ "4s", 11 },
	{ "5c", 12 },{ "5d", 13 },{ "5h", 14 },{ "5s", 15 },
	{ "6c", 16 },{ "6d", 17 },{ "6h", 18 },{ "6s", 19 },
	{ "7c", 20 },{ "7d", 21 },{ "7h", 22 },{ "7s", 23 },
	{ "8c", 24 },{ "8d", 25 },{ "8h", 26 },{ "8s", 27 },
	{ "9c", 28 },{ "9d", 29 },{ "9h", 30 },{ "9s", 31 },
	{ "Tc", 32 },{ "Td", 33 },{ "Th", 34 },{ "Ts", 35 },
	{ "Jc", 36 },{ "Jd", 37 },{ "Jh", 38 },{ "Js", 39 },
	{ "Qc", 40 },{ "Qd", 41 },{ "Qh", 42 },{ "Qs", 43 },
	{ "Kc", 44 },{ "Kd", 45 },{ "Kh", 46 },{ "Ks", 47 },
	{ "Ac", 48 },{ "Ad", 49 },{ "Ah", 50 },{ "As", 51 },
};




Card::Card(int num, char type) :num(num), type(type)
{
}

Card::Card() : Card(-1, 'u')
{
}


Card::~Card()
{
}

int Card::getNum() const
{
	return num;
}

char Card::getType() const
{
	return type;
}

std::string Card::toString() const
{
	return std::string(1, NUM_TO_CHAR[num]) +std::string(1, type);
}

int Card::toInt() const
{
	return CARD_ID.at(toString());
}


