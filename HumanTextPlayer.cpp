#include "HumanTextPlayer.h"
#include <iostream>
#include <windows.h>




HumanTextPlayer::HumanTextPlayer()
{
	std::cout << "HumanTextPlayer c'tor: " << getId() << std::endl;
}


HumanTextPlayer::~HumanTextPlayer()
{
}

void HumanTextPlayer::infoPlayerAction(int i, int action)
{
}

void HumanTextPlayer::InfoRoundStart()
{
	std::cout << "player" << getId() << " round start" << std::endl;
}

void HumanTextPlayer::InfoDealer()
{
	std::cout << "player" << getId() << " you are this round dealer" << std::endl;
}

void HumanTextPlayer::InfoBigBlind()
{
	std::cout << "player" << getId() << " you are this round big blind" << std::endl;
}

void HumanTextPlayer::InfoSmallBlind()
{
	std::cout << "player" << getId() << " you are this round small blind" << std::endl;
}

void HumanTextPlayer::InfoNormalPos()
{
	std::cout << "player" << getId() << " you are in a normal pos this round" << std::endl;
}

void HumanTextPlayer::InfoCardsDrown(const Card& card1, const Card& card2)
{
	std::cout << "player" << getId() << " your cards are " <<
		card1.toString() << ", " << card2.toString() << std::endl;
}

void HumanTextPlayer::MoneyPut(const int amount)
{
	std::cout << "player" << getId() << " you put " << amount << " on the pot" << std::endl;
}

int HumanTextPlayer::Play()
{
	int select = rand() % 5;
	switch (select)
	{
		case 0:
			return -1;
		case 1:
			return 0;
		case 2:
			return table->getCurrentBet() + table->getBigBlind(); // small raize
		case 3:
			return table->getCurrentBet() + table->getCurrentPot(); // mid raize (equal to pot
		case 4:
			return getMoney(); // all in
		default:
			return -1;
	}
}

void HumanTextPlayer::InfoReviealFlop(const std::vector<Card>& cards)
{
	std::cout << "player" << getId() << " the flop is: " <<
		cards[0].toString() << ", " <<
		cards[1].toString() << ", " <<
		cards[2].toString() << std::endl;
}

void HumanTextPlayer::InfoReviealTurn(const Card& card)
{
	std::cout << "player" << getId() << " the turn is: " <<
		card.toString() << std::endl;
}

void HumanTextPlayer::InfoReviealRiver(const Card& card)
{
	std::cout << "player" << getId()
		<< " the river is: " <<
		card.toString() << std::endl;
}

void HumanTextPlayer::InfoWinner(int winnerId)
{
	if (getId() == winnerId)
		std::cout << "player" << getId() << " you are this round's winner" << std::endl;
	else std::cout << "player" << getId() << " this round's winner is " << winnerId << std::endl;
}

void HumanTextPlayer::InfoShowDown(int playerId, int handRank)
{
}
