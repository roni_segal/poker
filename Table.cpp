#include "Table.h"
#include <thread>
#include <iostream>
#include <string>

// init static vars:
int Table::id_counter = 1;

Table::Table() : dealerIndex(-1), table_id(id_counter++), table_thread(&Table::GameLoop, this), start_game(false),
                 pot(0), currBet(0)
{
}

void Table::startGame()
{
	start_game = true;
}


Table::~Table()
{
	//TODO: info players
	closeTable = true;
	table_thread.join();
}

void Table::JoinPlayer(Player * new_player)
{
	players.push_back(new_player);
	new_player->sit(this);
}

std::vector<Card>& Table::getTableCards()
{
	return tableCards;
}

int Table::getBigBlind() const
{
	return bigBlind;
}

int Table::getCurrentPot() const
{
	return pot;
}

int Table::getCurrentBet() const
{
	return currBet;
}


void Table::GameLoop()
{
	while (start_game == false); // wait for start_game
	if (!closeTable)
	{
		dealerIndex = rand() % players.size();
	}
	int winner = -1;
	
	while (!closeTable)
	{
		bool wtsd = false;
		int folded = 0;
		// round:
		deck.shuffle();
		tableCards.clear();
		initRound();
		folded += betRound(bigBlind, dealerIndex + 2);
		if (players.size() - folded > 1)
		{
			revealFlop();
			folded += betRound(0, dealerIndex);
			if (players.size() - folded > 1)
			{
				reviealTurn();
				folded += betRound(0, dealerIndex);
				if (players.size() - folded > 1)
				{
					reviealRiver();
					folded += betRound(0, dealerIndex);
					if (players.size() - folded > 1)
					{
						wtsd = true;
						winner = showDown();
					}
				}
			}
		}

		if (!wtsd) // didn't went to show down (one player left)
		{
			for (int i = 0; i < players.size(); i++)
			{
				if (players[i]->isFold() == false)
					winner = i;
			}
		}

		for (int i = 0; i < players.size(); i++)
		{
			players[i]->infoWinner(winner);
		}

		dealerIndex = (dealerIndex + 1) % players.size();
	}
}

// card destrebution, dealer, small&big blinds
void Table::initRound()
{
	pot = 0;
	for (int i = 0; i < players.size(); i++)
	{
		players[i]->infoRoundStart();
		players[i]->infoCardsDrown(deck.dealCard(), deck.dealCard());
	}

	players[dealerIndex % players.size()]->infoDealer();

	players[(dealerIndex + 1) % players.size()]->putMoney(smallBlind);
	players[(dealerIndex + 1) % players.size()]->infoSmallBlind();

	players[(dealerIndex + 2) % players.size()]->putMoney(bigBlind);
	players[(dealerIndex + 2) % players.size()]->infoBigBlind();

	for(int i = dealerIndex + 3; (i % players.size()) != dealerIndex; i++)
	{
		players[i % players.size()]->infoNormalPos();
	}
}


int Table::betRound(int currentBet, int startPos)
{
	currBet = currentBet;
	int folded = 0;
	int last_raize_index = startPos % players.size();
	int i = (last_raize_index + 1) % players.size();
	while(last_raize_index != i % players.size())
	{
		if (!players[i]->isFold())
		{
			const int action = players[i]->play(); // -1 fold 0 check/call x raize
			for (int j = 0; j < players.size(); j++)
			{
				players[j]->infoPlayerAction(i, action);
			}

			if (action == -1) { folded++; } // fold
			else if(action == 0) // call
			{
				players[i]->putMoney(currBet);
			}
			else // raize
			{
				if(action > currBet)
				{
					currBet = action;
					players[i]->putMoney(currBet);
					last_raize_index = i;
				}
				else
				{
					tableLog("invalid player play. raize of lower amount then call");
				}
			}
		}
		i++;
		i = i % players.size();
	}
	return folded;
}

void Table::revealFlop()
{
	tableCards.push_back(deck.dealCard());
	tableCards.push_back(deck.dealCard());
	tableCards.push_back(deck.dealCard());
	for(int i = 0; i < players.size(); i++)
	{
		players[i]->infoReviealFlop(tableCards);
	}
}

void Table::reviealTurn()
{
	tableCards.push_back(deck.dealCard());
	for(int i = 0; i < players.size(); i++)
	{
		players[i]->infoReviealTurn(tableCards[3]);
	}
}

void Table::reviealRiver()
{
	tableCards.push_back(deck.dealCard());
	for(int i = 0; i < players.size(); i++)
	{
		players[i]->infoReviealRiver(tableCards[tableCards.size() - 1]);
	}
}

int Table::showDown()
{
	int winner = -1;
	int currMax = INT_MIN;
	for(int i = 0; i < players.size(); i++)
	{
		if(players[i]->isFold() == false)
		{
			const int currHandRank = players[i]->getHandRank();
			if(currHandRank > currMax)
			{
				currMax = currHandRank;
				winner = i;
			}
			players[i]->infoShowDown(i, players[i]->getHandRank());
		}
	}
	return winner;
}

int Table::checkWinner()
{
	int max = INT_MIN;
	int winner = -1;
	for (int i = 0; i < players.size(); i++)
	{
		if (players[i]->isFold() == false)
		{
			const int current_rank = players[i]->getHandRank();
			if (current_rank > max)
			{
				winner = i;
				max = current_rank;
			}
		}
	}
	tableLog("The winner of the round is: " + std::to_string(players[winner]->getId()));
	for (int i = 0; i < players.size(); i++)
	{
		players[i]->infoWinner(players[winner]->getId());
	}
	players[winner]->takeMoney(pot);
	return winner;
}

void Table::tableLog(std::string msg)
{
	std::cout << "table: " << msg << std::endl;
}

