#pragma once
#include <string>
#include <unordered_map>

class Card
{
public:
	Card(int num, char type);
	Card();
	~Card();
	
	int getNum() const;
	char getType() const;
	std::string toString() const;
	int toInt() const;

private:
	int num;
	char type;

	static const std::unordered_map<std::string, int> CARD_ID;
	const static char NUM_TO_CHAR[];
};
