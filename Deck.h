#pragma once
#include "Card.h"
#include <vector>

class Deck
{
public:
	Deck();
	void shuffle();
	Card dealCard();
	~Deck();

private:
	std::vector<Card> deck;
	const int numOfCards = 52;
	const char types[4] = { 's', 'c', 'h', 'd' };
	int currentCard;
};

