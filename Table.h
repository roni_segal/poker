#pragma once
#include <vector>
#include "Player.h"
#include "Deck.h"
#include <thread>

class Player;

class Table
{
public:
	Table();
	void startGame();
	~Table();
	void JoinPlayer(Player* new_player);
	std::vector<Card>& getTableCards();
	int getBigBlind() const;
	int getCurrentPot() const;
	int getCurrentBet() const;

private:
	std::vector<Player*> players;
	void initRound();
	int betRound(int currBet, int startPos);
	void revealFlop();
	void reviealTurn();
	void reviealRiver();
	int showDown();
	int checkWinner();
	void GameLoop();
	static void tableLog(std::string msg); 
	bool closeTable = false;
	int dealerIndex;
	Deck deck;
	const int smallBlind = 50;
	const int bigBlind = 100;
	std::vector<Card> tableCards;
	int table_id;
	static int id_counter;
	std::thread table_thread;
	bool start_game = false;

	// round vars:
	int pot;
	int currBet;
};

