#include "Deck.h"
#include <algorithm>


Deck::Deck(): currentCard(0)
{
	for(int j = 0; j < sizeof(types) / sizeof(char); j++)
	{
		for (int i = 0; i < 13; i++)
		{
			deck.push_back(Card(i, types[j]));
		}
	}
	
}

void Deck::shuffle()
{
	std::random_shuffle(deck.begin(), deck.end());
	currentCard = 0;
}

Card Deck::dealCard()
{
	return deck.at(currentCard++ % numOfCards);
}


Deck::~Deck()
{
}
