


#include "Table.h"
#include "HumanTextPlayer.h"
#include "GUIPlayer.h"
#include <iostream>

int main(int argc, char** argv)
{
	Table table;

	table.JoinPlayer(new GuiPlayer());
	table.JoinPlayer(new GuiPlayer());
	table.JoinPlayer(new GuiPlayer());
	table.JoinPlayer(new GuiPlayer());

	table.startGame();

	while (true);
	return 0;
}
