#include "GUIPlayer.h"
#include <thread>


GuiPlayer::GuiPlayer() :
	Player(), window(nullptr), table_cards_changed(false),
	window_thread(&GuiPlayer::GameLoop, this)
{
	font.loadFromFile("arial.ttf");
	textGameState.setFont(font);
	textGameState.setCharacterSize(30);
	textGameState.setFillColor(sf::Color::White);
}

void GuiPlayer::drawMyCards()
{
	window->draw(card1Sprite);
	window->draw(card2Sprite);
}

void GuiPlayer::drawTableCards()
{
	if(table_cards_changed)
	{
		table_cards_sprites.clear();
		table_cards_texture.clear();
		for(int i = 0; i < table_cards.size(); i++)
		{
			table_cards_texture.push_back(sf::Texture());
			table_cards_texture[table_cards_texture.size() - 1].loadFromFile("cards\\" + table_cards[i].toString() + ".PNG");
			table_cards_sprites.push_back(sf::Sprite(table_cards_texture[table_cards_texture.size() - 1]));
			table_cards_sprites[table_cards_sprites.size() - 1].setPosition(250 + i * 40, 250);
		}
		table_cards_changed = false;
	}

	for(int i = 0; i < table_cards_sprites.size(); i++)
	{
		window->draw(table_cards_sprites[i]);
	}
}

void GuiPlayer::draw()
{
	if(window != nullptr)
	{
		window->draw(textGameState);
		drawMyCards();
		drawTableCards();
	}
}

void GuiPlayer::GameLoop()
{
	window = new sf::RenderWindow(sf::VideoMode(800, 600), "player" + std::to_string(getId()));
	textGameState.setPosition(window->getSize().x / 2, 0);
	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close(); // TODO: disconnect from the table
		}
		window->clear();
		draw();
		window->display();
	}
}



GuiPlayer::~GuiPlayer()
{
	window->close();
	window_thread.join();
	delete window;
}

void GuiPlayer::infoPlayerAction(int i, int action)
{
}

void GuiPlayer::InfoWinner(int winnerId)
{
}

void GuiPlayer::InfoRoundStart()
{
	textGameState.setString("round start");
	table_cards_changed = true; // clean the old cards
}

void GuiPlayer::InfoNormalPos()
{
}

void GuiPlayer::InfoDealer()
{
}

void GuiPlayer::InfoBigBlind()
{
}

void GuiPlayer::InfoSmallBlind()
{
}

void GuiPlayer::InfoReviealFlop(const std::vector<Card>& cards)
{
	textGameState.setString("post flop");
	table_cards.clear();
	table_cards.push_back(cards[0]);
	table_cards.push_back(cards[1]);
	table_cards.push_back(cards[2]);
	table_cards_changed = true;
}

void GuiPlayer::InfoReviealTurn(const Card& card)
{
	textGameState.setString("post turn");
	table_cards.push_back(card);
	table_cards_changed = true;
}

void GuiPlayer::InfoReviealRiver(const Card& card)
{
	textGameState.setString("post river");
	table_cards.push_back(card);
	table_cards_changed = true;
}

void GuiPlayer::InfoCardsDrown(const Card& card1, const Card& card2)
{
	card1Texture.loadFromFile("cards\\" + card1.toString() + ".PNG");
	card2Texture.loadFromFile("cards\\" + card2.toString() + ".PNG");
	card1Sprite.setTexture(card1Texture);
	card2Sprite.setTexture(card2Texture);
	if(window != nullptr)
	{
		card1Sprite.setPosition(window->getSize().x / 2 - card1Texture.getSize().x / 2, window->getSize().y - card1Texture.getSize().y);
		card2Sprite.setPosition(window->getSize().x / 2 + card2Texture.getSize().x / 2, window->getSize().y - card1Texture.getSize().y);
	}
}

void GuiPlayer::MoneyPut(const int small_blind)
{
}

int GuiPlayer::Play()
{
	for (int i = 0; i < 10000; i++)
		for (int j = 0; j < 20000; j++);
	return 0;
}

void GuiPlayer::InfoShowDown(int playerId, int handRank)
{
}