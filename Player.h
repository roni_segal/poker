#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "Card.h"
#include <vector>
#include "Table.h"

class Table;

class Player
{
public:
	Player();
	virtual ~Player();
	void sit(Table* table);
	void infoDealer();
	void infoBigBlind();
	void infoSmallBlind();
	void infoNormalPos();
	void infoRoundStart();
	void infoCardsDrown(Card card1, Card card2);
	void putMoney(const int amount);
	void takeMoney(const int amount);
	int play();
	virtual void infoPlayerAction(int i, int action) = 0;
	void infoReviealFlop(const std::vector<Card>& cards);
	void infoReviealTurn(const Card& card);
	void infoReviealRiver(const Card& card);
	int getHandRank() const;

	bool isFold() const;
	bool isDealer() const;
	bool isBigBlind() const;
	bool isSmallBlind() const;
	int getDealerCount() const;
	int getBigBlindsCount() const;
	int getSmallBlindsCount() const;

	bool isFlopRevieald() const;
	bool isTurnRevieald() const;
	bool isRiverRevieald() const;
	int getId() const;
	void infoWinner(int winnerId);
	int getMoney() const;
	void infoShowDown(int playerId, int handRank);


protected:
	virtual void InfoWinner(int winnerId) = 0;
	virtual void InfoRoundStart() = 0;
	virtual void InfoNormalPos() = 0;
	virtual void InfoDealer() = 0;
	virtual void InfoBigBlind() = 0;
	virtual void InfoSmallBlind() = 0;
	virtual void InfoReviealFlop(const std::vector<Card>& cards) = 0;
	virtual void InfoReviealTurn(const Card& card) = 0;
	virtual void InfoReviealRiver(const Card& card) = 0;
	virtual void InfoCardsDrown(const Card& card, const Card& deal_card) = 0;
	virtual void MoneyPut(const int small_blind) = 0;
	virtual int Play() = 0;
	virtual void InfoShowDown(int playerId, int handRank) = 0;
	Table* table;

private:
	void Fold();
	int dealerCount;
	int bigBlindCount;
	int smallBlindCount;

	bool is_fold;
	bool is_dealer;
	bool is_big_blind;
	bool is_small_blind;
	bool is_normal_pos;


	bool is_flop_revieald;
	bool is_turn_revieald;
	bool is_river_revieald;

	int fold_on_river;
	int fold_on_turn;
	int fold_on_flop;
	int fold_on_hand;


	Card hand_card1;
	Card hand_card2;
	int roundsPlayed;
	int money;
	int id;
	const int startMoney = 10000;

	static int HR[32487834];
	static bool isHrInit;
	static int LookupHand(int* pCards);
	static int player_id_counter;

};
